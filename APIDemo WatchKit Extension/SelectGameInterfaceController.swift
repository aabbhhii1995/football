//
//  SelectGameInterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Abhishek Bansal on 2019-07-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation


class SelectGameInterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
