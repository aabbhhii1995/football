//
//  ThirdPlaceInterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Abhishek Bansal on 2019-07-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON


class ThirdPlaceInterfaceController: WKInterfaceController {
    
    
    var teamA = [""]
    var teamB = [""]
    
    @IBOutlet var thirdPlaceTable: WKInterfaceTable!
    
    @IBOutlet var titleLabel: WKInterfaceLabel!
    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        print("Third Place screen loaded")
        
        
        let URL = "https://fbase-example-157f9.firebaseio.com/.json"
        print("Url: \(URL)")
        Alamofire.request(URL).responseJSON {
            // 1. store the data from the internet in the
            // response variable
            response in
            
            // 2. get the data out of the variable
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }
            // GET sunrise/sunset time out of the JSON response
            let jsonResponse = JSON(apiData)
            
            var responseArray = jsonResponse.dictionaryValue
            
            let item = responseArray;
            print(item)
            self.titleLabel.setText("THIRD PLACE")
            //stadium
            let teamA0 = responseArray["Third-Place"]?[0]["TeamA"];
           
            
            // print(stadium0!)
            
            self.teamA = [teamA0?.string] as! [String]
            print(self.teamA)
            
            //place
            let teamB0 = responseArray["Third-Place"]?[0]["TeamB"];
            self.teamB = [teamB0?.string] as! [String]
            print(self.teamB)
            
            
            
            self.thirdPlaceTable.setNumberOfRows(self.teamA.count, withRowType: "myRow")
            
            for (i, product) in self.teamA.enumerated() {
                print(product)
                
                var row = self.thirdPlaceTable.rowController(at: i) as! SheduleRowController
                print("row label",row.tpTeamALabel)
                row.tpTeamALabel.setText(product)
                print("i == ",i)
            }
            for (j, product1) in self.teamB.enumerated() {
                print(product1)
                
                //let s = Int(i);
                let row = self.thirdPlaceTable.rowController(at: j) as! SheduleRowController
                print("i2 == ",j)
                row.tpTeamBLabel.setText(product1)
            }
            
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
