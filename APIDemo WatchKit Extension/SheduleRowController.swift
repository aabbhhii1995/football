//
//  SheduleRowController.swift
//  APIDemo WatchKit Extension
//
//  Created by Abhishek Bansal on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit

class SheduleRowController: NSObject {
    
    @IBOutlet var stadiumLabel: WKInterfaceLabel!
    
    
    @IBOutlet var placeLabel: WKInterfaceLabel!
    
    
    @IBOutlet var qfTeamALabel: WKInterfaceLabel!
    
    
    @IBOutlet var qfTeamBLabel: WKInterfaceLabel!
    
    
    @IBOutlet var tpTeamALabel: WKInterfaceLabel!
    
    @IBOutlet var tpTeamBLabel: WKInterfaceLabel!
    
    @IBOutlet var fTeamALabel: WKInterfaceLabel!
    
    @IBOutlet var fTeamBLabel: WKInterfaceLabel!
    
    
}
