//
//  QuarterFinalInterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Abhishek Bansal on 2019-07-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON


class QuarterFinalInterfaceController: WKInterfaceController {
    
    var teamA = [""]
    var teamB = [""]
    
    
    @IBOutlet var titleLabel: WKInterfaceLabel!
    
    @IBOutlet var quarterFinalTab: WKInterfaceTable!
    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        print("Quarter Final screen loaded")
        
        
        let URL = "https://fbase-example-157f9.firebaseio.com/.json"
        print("Url: \(URL)")
        Alamofire.request(URL).responseJSON {
            // 1. store the data from the internet in the
            // response variable
            response in
            
            // 2. get the data out of the variable
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }
            // GET sunrise/sunset time out of the JSON response
            let jsonResponse = JSON(apiData)
            
            var responseArray = jsonResponse.dictionaryValue
            
            let item = responseArray;
            print(item)
            self.titleLabel.setText("SEMIFINALS")
            //stadium
            let teamA0 = responseArray["Semi-Final"]?[0]["TeamA"];
            let teamA1 = responseArray["Semi-Final"]?[1]["TeamA"];
            
           // print(stadium0!)
            
            self.teamA = [teamA0?.string , teamA1?.string] as! [String]
            print(self.teamA)
            
            //place
            let teamB0 = responseArray["Semi-Final"]?[0]["TeamB"];
            let teamB1 = responseArray["Semi-Final"]?[1]["TeamB"];
            
            
            
            self.teamB = [teamB0?.string , teamB1?.string] as! [String]
            print(self.teamB)
            
            
            
            self.quarterFinalTab.setNumberOfRows(self.teamA.count, withRowType: "myRow")
            
            for (i, product) in self.teamA.enumerated() {
                print(product)
                
                var row = self.quarterFinalTab.rowController(at: i) as! SheduleRowController
                print("row label",row.qfTeamALabel)
                row.qfTeamALabel.setText(product)
                print("i == ",i)
            }
            for (j, product1) in self.teamB.enumerated() {
                print(product1)
                
                //let s = Int(i);
                let row = self.quarterFinalTab.rowController(at: j) as! SheduleRowController
                print("i2 == ",j)
                row.qfTeamBLabel.setText(product1)
            }
            
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
