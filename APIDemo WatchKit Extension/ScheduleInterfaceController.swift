//
//  ScheduleInterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Abhishek Bansal on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON

class ScheduleInterfaceController: WKInterfaceController {

    var stadiums = [""]
    var places = [""]
    
    //MARK: outlet
    
    @IBOutlet var SheduleTable: WKInterfaceTable!
    
    
    @IBOutlet var titleLabel: WKInterfaceLabel!
    
    @IBOutlet var subscribeLabel: WKInterfaceLabel!
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
       
        print("Schedule screen loaded")
        
        
        let URL = "https://fbase-example-157f9.firebaseio.com/.json"
        print("Url: \(URL)")
        Alamofire.request(URL).responseJSON {
            // 1. store the data from the internet in the
            // response variable
            response in
            
            // 2. get the data out of the variable
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }
            // GET sunrise/sunset time out of the JSON response
            let jsonResponse = JSON(apiData)
            
            var responseArray = jsonResponse.dictionaryValue
            
            let item = responseArray;
            print(item)
            self.titleLabel.setText("QUARTERFINALS")
            //titleLabel.Int
            
            //stadium
            let stadium0 = responseArray["QuarterFinal"]?[0]["TeamA0"];
            let stadium1 = responseArray["QuarterFinal"]?[1]["TeamA1"];
            let stadium2 = responseArray["QuarterFinal"]?[2]["TeamA2"];
            let stadium3 = responseArray["QuarterFinal"]?[3]["TeamA3"];
            print(stadium0!)
            
            self.stadiums = [stadium0?.string , stadium1?.string , stadium2?.string , stadium3?.string] as! [String]
            print(self.stadiums)
            
            //place
            let place0 = responseArray["QuarterFinal"]?[0]["TeamB0"];
            let place1 = responseArray["QuarterFinal"]?[1]["TeamB1"];
            let place2 = responseArray["QuarterFinal"]?[2]["TeamB2"];
            let place3 = responseArray["QuarterFinal"]?[3]["TeamB3"];
            
            
            self.places = [place0?.string , place1?.string , place2?.string , place3?.string] as! [String]
            print(self.places)
            
            
            self.SheduleTable.setNumberOfRows(self.stadiums.count, withRowType: "myRow")
            
            for (i, product) in self.stadiums.enumerated() {
                print(product)
                
                var row = self.SheduleTable.rowController(at: i) as! SheduleRowController
                print("row label",row.stadiumLabel)
                row.stadiumLabel.setText(product)
                print("i == ",i)
            }
            for (j, product1) in self.places.enumerated() {
                print(product1)
                
                //let s = Int(i);
                let row = self.SheduleTable.rowController(at: j) as! SheduleRowController
                                print("i2 == ",j)
                row.placeLabel.setText(product1)
            }
            
        }
    }
    

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
