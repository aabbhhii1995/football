//
//  ViewController.swift
//  APIDemo
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    //MARK: Outlet
    
    
    @IBOutlet weak var showImage: UIImageView!
    
    
    
    
    override func viewDidLoad() {
        
        self.navigationItem.title = "FIFA Women's World Cup France 2019"
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
       
        }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: BUTTONS
    
    @IBAction func mapButton(_ sender: Any) {
        
        print("Map Button pressed")
        
    }
    
    @IBAction func gameScheduleButton(_ sender: Any) {
        
         print("Game Schedule Button pressed")
    }
    

}

