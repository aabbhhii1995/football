//
//  ThirdPlaceViewController.swift
//  APIDemo
//
//  Created by Abhishek Bansal on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ThirdPlaceViewController: UIViewController {

    //MARK: Outlets
    
    //Title
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    @IBOutlet weak var imgTeamA: UIImageView!
    
    @IBOutlet weak var imgTeamB: UIImageView!
    
    @IBOutlet weak var teamALabel: UILabel!
    
    @IBOutlet weak var teamBLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var stadiumLabel: UILabel!
    
    @IBOutlet weak var placeLabel: UILabel!
    
    
    @IBOutlet weak var subscribeLabel: UILabel!
    
    
    // MARK: Button
    @IBAction func subscribeButton(_ sender: Any) {
        print("pressed")
        alert()
        if(subscribeLabel.text == "Subscribe")
        {
            print("yes 1")
            self.subscribeLabel.text = "Unsubscribe"
            print("Subscribed succefully")
            
        }
        else if(subscribeLabel.text == "Unsubscribe")
        {
            print("yes 2")
            self.subscribeLabel.text = "Subscribe"
            print("Unsubscribed succefully")
        }
    }
    
    
    
    
    override func viewDidLoad() {
        self.navigationItem.title = "Third Place"
        super.viewDidLoad()

       parseData()
    }
    
    func alert(){
        var refreshAlert = UIAlertView()
        refreshAlert.title = subscribeLabel.text!
        refreshAlert.message = "You have \(subscribeLabel.text!)d the game sucessfully"
        refreshAlert.addButton(withTitle: "OK")
        refreshAlert.show()
    }

    func parseData() {
        let URL = "https://fbase-example-157f9.firebaseio.com/.json"
        
        Alamofire.request(URL).responseJSON {
            
            response in
            let apiData = response.result.value
            if (apiData == nil) {
                print("Error when getting API data")
                return
            }
            let jsonResponse = JSON(apiData)
            var responseArray = jsonResponse.dictionaryValue
            
            let item = responseArray;
            self.titleLabel.text! = "THIRD PLACE"
            
            
            let stadium = responseArray["Third-Place"]?[0]["Stadium"];
            let date = responseArray["Third-Place"]?[0]["Date"];
            let time = responseArray["Third-Place"]?[0]["Time"];
            let place = responseArray["Third-Place"]?[0]["Place"];
            let teamA = responseArray["Third-Place"]?[0]["TeamA"];
            let teamB = responseArray["Third-Place"]?[0]["TeamB"];
            
            // checking the output
            print(stadium!)
            print(date!)
            print(time!)
            print(place!)
            print(teamA!)
            print(teamB!)
            
            //setting in labels
            self.stadiumLabel.text! = "\(stadium!)"
            self.dateLabel.text! = "\(date!)"
            self.timeLabel.text! = "\(time!)"
            self.placeLabel.text! = "\(place!)"
            self.imgTeamA.image = UIImage(named: "england")
            self.imgTeamB.image = UIImage(named: "sweden")
            self.teamALabel.text! = "\(teamA!)"
            self.teamBLabel.text! = "\(teamB!)"
            
            
            
            
        }
        //let stadiums = [stadium0, stadium1, stadium2, stadium3]
    }

}
