//
//  QuarterFinalViewController.swift
//  APIDemo
//
//  Created by Abhishek Bansal on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class QuarterFinalViewController: UIViewController {
    
    //MARK: outlet
    
    @IBOutlet weak var titleLabel: UILabel!
    
    // first
    
    @IBOutlet weak var date0Label: UILabel!
    @IBOutlet weak var time0Label: UILabel!
    @IBOutlet weak var stadium0Label: UILabel!
    @IBOutlet weak var place0Label: UILabel!
    @IBOutlet weak var imgTeamA0: UIImageView!
    @IBOutlet weak var imgTeamB0: UIImageView!
    @IBOutlet weak var teamALabel: UILabel!
    @IBOutlet weak var teamBLabel: UILabel!
    
    // second
    @IBOutlet weak var date1Label: UILabel!
    @IBOutlet weak var time1Label: UILabel!
    @IBOutlet weak var stadium1Label: UILabel!
    @IBOutlet weak var place1Label: UILabel!
    @IBOutlet weak var teamA1Label: UILabel!
    @IBOutlet weak var teamB1Label: UILabel!
    @IBOutlet weak var imgTeamA1: UIImageView!
    @IBOutlet weak var imgTeamB1: UIImageView!
    
    
    // third
    @IBOutlet weak var date2Label: UILabel!
    @IBOutlet weak var time2Label: UILabel!
    @IBOutlet weak var stadium2Label: UILabel!
    @IBOutlet weak var place2Label: UILabel!
    @IBOutlet weak var teamA2Label: UILabel!
    @IBOutlet weak var teamB2Label: UILabel!
    @IBOutlet weak var imgTeamA2: UIImageView!
    @IBOutlet weak var imgTeamB2: UIImageView!
    
    // fourth
    @IBOutlet weak var date3Label: UILabel!
    
    @IBOutlet weak var time3Label: UILabel!
    @IBOutlet weak var stadium3Label: UILabel!
    @IBOutlet weak var place3Label: UILabel!
    @IBOutlet weak var teamA3Label: UILabel!
    @IBOutlet weak var teamB3Label: UILabel!
    @IBOutlet weak var imgTeamA3: UIImageView!
    @IBOutlet weak var imgTeamB3: UIImageView!
    
    
    //MARK: buttons
    
    
    
   
    
    
    override func viewDidLoad() {
        self.navigationItem.title = "Quarter Finals"
        super.viewDidLoad()
        parseData()

    }
    
    
    
    
    func parseData() {
        let URL = "https://fbase-example-157f9.firebaseio.com/.json"
        
        Alamofire.request(URL).responseJSON {
            response in
            let apiData = response.result.value
            if (apiData == nil) {
                print("Error when getting API data")
                return
            }
            let jsonResponse = JSON(apiData)
            
            var responseArray = jsonResponse.dictionaryValue
            
            let item = responseArray;
            //print(item)
            self.titleLabel.text! = "QUARTER FINALS"
            
            
            //FIRST
            let stadium0 = responseArray["QuarterFinal"]?[0]["Stadium0"];
            let date0 = responseArray["QuarterFinal"]?[0]["Date0"];
            let time0 = responseArray["QuarterFinal"]?[0]["Time0"];
            let place0 = responseArray["QuarterFinal"]?[0]["Place0"];
            let teamA = responseArray["QuarterFinal"]?[0]["TeamA0"];
            let teamB = responseArray["QuarterFinal"]?[0]["TeamB0"];
            
            // checking the output
            print(stadium0!)
            print(date0!)
            print(time0!)
            print(place0!)
            print(teamA!)
            print(teamB!)
            
            //setting in labels
            self.stadium0Label.text! = "\(stadium0!)"
            self.date0Label.text! = "\(date0!)"
            self.time0Label.text! = "\(time0!)"
            self.place0Label.text! = "\(place0!)"
            self.imgTeamA0.image = UIImage(named: "norway")
            self.imgTeamB0.image = UIImage(named: "england")
            self.teamALabel.text! = "\(teamA!)"
            self.teamBLabel.text! = "\(teamB!)"
            
            
            // SECOND
            let stadium1 = responseArray["QuarterFinal"]?[1]["Stadium1"];
            let date1 = responseArray["QuarterFinal"]?[1]["Date1"];
            let time1 = responseArray["QuarterFinal"]?[1]["Time1"];
            let place1 = responseArray["QuarterFinal"]?[1]["Place1"];
            let teamA1 = responseArray["QuarterFinal"]?[1]["TeamA1"];
            let teamB1 = responseArray["QuarterFinal"]?[1]["TeamB1"];
            
            //setting in labels
            self.stadium1Label.text! = "\(stadium1!)"
            self.date1Label.text! = "\(date1!)"
            self.time1Label.text! = "\(time1!)"
            self.place1Label.text! = "\(place1!)"
            self.imgTeamA1.image = UIImage(named: "france")
            self.imgTeamB1.image = UIImage(named: "united-states")
            self.teamA1Label.text! = "\(teamA1!)"
            self.teamB1Label.text! = "\(teamB1!)"
            
            
            //THIRD
            let stadium2 = responseArray["QuarterFinal"]?[2]["Stadium2"];
            let date2 = responseArray["QuarterFinal"]?[2]["Date2"];
            let time2 = responseArray["QuarterFinal"]?[2]["Time2"];
            let place2 = responseArray["QuarterFinal"]?[2]["Place2"];
            let teamA2 = responseArray["QuarterFinal"]?[2]["TeamA2"];
            let teamB2 = responseArray["QuarterFinal"]?[2]["TeamB2"];
            
            //setting in labels
            self.stadium2Label.text! = "\(stadium2!)"
            self.date2Label.text! = "\(date2!)"
            self.time2Label.text! = "\(time2!)"
            self.place2Label.text! = "\(place2!)"
            self.imgTeamA2.image = UIImage(named: "italy")
            self.imgTeamB2.image = UIImage(named: "netherlands")
            self.teamA2Label.text! = "\(teamA2!)"
            self.teamB2Label.text! = "\(teamB2!)"
            
            
            //FOURTH
            let stadium3 = responseArray["QuarterFinal"]?[3]["Stadium3"];
            let date3 = responseArray["QuarterFinal"]?[3]["Date3"];
            let time3 = responseArray["QuarterFinal"]?[3]["Time3"];
            let place3 = responseArray["QuarterFinal"]?[3]["Place3"];
            let teamA3 = responseArray["QuarterFinal"]?[3]["TeamA3"];
            let teamB3 = responseArray["QuarterFinal"]?[3]["TeamB3"];
            
            //setting in labels
            self.stadium3Label.text! = "\(stadium3!)"
            self.date3Label.text! = "\(date3!)"
            self.time3Label.text! = "\(time3!)"
            self.place3Label.text! = "\(place3!)"
            self.imgTeamA3.image = UIImage(named: "germany")
            self.imgTeamB3.image = UIImage(named: "sweden")
            self.teamA3Label.text! = "\(teamA3!)"
            self.teamB3Label.text! = "\(teamB3!)"
            
        }
        //let stadiums = [stadium0, stadium1, stadium2, stadium3]
    }
    
    
    
    
    
    
   
}
