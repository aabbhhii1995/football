//
//  SelectViewController.swift
//  APIDemo
//
//  Created by Abhishek Bansal on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit

class SelectViewController: UIViewController {

    override func viewDidLoad() {
        self.navigationItem.title = "Schedule"
        super.viewDidLoad()

        
        
        
        
    }
    
    //MARK: Buttons
    
    @IBAction func quarterFinalButton(_ sender: Any) {
        
        print("Quarter Final button pressed")
        

    }
    
    @IBAction func semiFinalButton(_ sender: Any) {
        
        print("Semi Final button pressed")
        
    }
    
    @IBAction func thirdPlaceButton(_ sender: Any) {
        
        print("Third Place button pressed")
        
    }
    
    
    @IBAction func finalButton(_ sender: Any) {
        
        print("Final button pressed")
        
    }
    

}
