//
//  SheduleViewController.swift
//  APIDemo
//
//  Created by Abhishek Bansal on 2019-06-29.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit
import CoreLocation


class SheduleViewController: UIViewController {
    
    //MARK: Label
    
    @IBOutlet weak var QuarterFinalTableView: UITableView!
    
    
    
    
    //var fetchedQuarterFinal = [QuarterFinal]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //QuarterFinalTableView.dataSource = self
        parseData()
       }
    
    // MARK: Functions
    
    
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        //return stadiums.count
//        return 0
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        return
//    }
    
    func parseData() {
        let URL = "https://fbase-example-157f9.firebaseio.com/.json"
        
        Alamofire.request(URL).responseJSON {
            
            response in
            
            // TODO: Put your code in here
            // ------------------------------------------
            // 1. Convert the API response to a JSON object
            
            // -- check for errors
            let apiData = response.result.value
            if (apiData == nil) {
                print("Error when getting API data")
                return
            }
            // -- if no errors, then keep going
            
            //print(apiData)
            
            
            // 2. Parse out the data you need (sunrise / sunset time)
            
            // example2 - parse an array of dictionaries
            
            // 2a. Convert the response to a JSON object
            let jsonResponse = JSON(apiData)
            
            //print(jsonResponse)
            
            // 2b. Get the array out of the JSON object
            var responseArray = jsonResponse.dictionaryValue
            
            
            
            let item = responseArray;
            //print(item)
            
            let stadium0 = responseArray["QuarterFinal"]?[0]["Stadium0"];
            // checking the output
            print(stadium0)
            
            let stadium1 = responseArray["QuarterFinal"]?[1]["Stadium1"];
            print(stadium1)
            let stadium2 = responseArray["QuarterFinal"]?[2]["Stadium2"];
            print(stadium2)
            let stadium3 = responseArray["QuarterFinal"]?[3]["Stadium3"];
            print(stadium3)
            
            
           
    }
        //let stadiums = [stadium0, stadium1, stadium2, stadium3]
}

}
