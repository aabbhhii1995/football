//
//  SemiFinalViewController.swift
//  APIDemo
//
//  Created by Abhishek Bansal on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import WatchConnectivity


class SemiFinalViewController: UIViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    //MARK: Outlet
    
    //Title
    @IBOutlet weak var titleLabel: UILabel!
    //First
    @IBOutlet weak var date0Label: UILabel!
    @IBOutlet weak var time0Label: UILabel!
    @IBOutlet weak var stadium0Label: UILabel!
    
    @IBOutlet weak var place0Label: UILabel!
    
    @IBOutlet weak var teamA0Label: UILabel!
    
    @IBOutlet weak var teamB0Label: UILabel!
    
    @IBOutlet weak var imgTeamA0: UIImageView!
    @IBOutlet weak var imgTeamB0: UIImageView!
    //Second
    @IBOutlet weak var date1Label: UILabel!
    @IBOutlet weak var time1Label: UILabel!
    @IBOutlet weak var stadium1Label: UILabel!
    @IBOutlet weak var place1Label: UILabel!
    @IBOutlet weak var teamA1Label: UILabel!
    @IBOutlet weak var teamB1Label: UILabel!
    
    @IBOutlet weak var imgTeamA1: UIImageView!
    @IBOutlet weak var imgTeamB1: UIImageView!
    
    
    
    @IBOutlet weak var subscribeLabel: UILabel!
    
    
    
    //MARK: Button
    
    @IBAction func subscribeButton(_ sender: Any) {
        print("pressed")
        alert()
    if(subscribeLabel.text == "Subscribe")
    {
        print("yes 1")
        self.subscribeLabel.text = "Unsubscribe"
        print("Subscribed succefully")
    }
        else if(subscribeLabel.text == "Unsubscribe")
        {
            print("yes 2")
            self.subscribeLabel.text = "Subscribe"
            print("Unsubscribed succefully")
        }
    }
    
    override func viewDidLoad() {
        self.navigationItem.title = "SemiFinals"
        super.viewDidLoad()
         parseData()
    }
    
    func alert(){
        var refreshAlert = UIAlertView()
        refreshAlert.title = subscribeLabel.text!
        refreshAlert.message = "You have \(subscribeLabel.text!)d the game sucessfully"
        refreshAlert.addButton(withTitle: "OK")
        refreshAlert.show()
    }
    

    func parseData() {
        let URL = "https://fbase-example-157f9.firebaseio.com/.json"
        
        Alamofire.request(URL).responseJSON {
            
            response in
            let apiData = response.result.value
            if (apiData == nil) {
                print("Error when getting API data")
                return
            }
            let jsonResponse = JSON(apiData)
            var responseArray = jsonResponse.dictionaryValue
            
            let item = responseArray;
            self.titleLabel.text! = "SEMI FINALS"
            
            //FIRST
            let stadium0 = responseArray["Semi-Final"]?[0]["Stadium"];
            let date0 = responseArray["Semi-Final"]?[0]["Date"];
            let time0 = responseArray["Semi-Final"]?[0]["Time"];
            let place0 = responseArray["Semi-Final"]?[0]["Place"];
            let teamA0 = responseArray["Semi-Final"]?[0]["TeamA"];
            let teamB0 = responseArray["Semi-Final"]?[0]["TeamB"];
            
            // checking the output
            print(stadium0!)
            print(date0!)
            print(time0!)
            print(place0!)
            print(teamA0!)
            print(teamB0!)
            
            //setting in labels
            self.stadium0Label.text! = "\(stadium0!)"
            self.date0Label.text! = "\(date0!)"
            self.time0Label.text! = "\(time0!)"
            self.place0Label.text! = "\(place0!)"
            self.imgTeamA0.image = UIImage(named: "england")
            self.imgTeamB0.image = UIImage(named: "united-states")
            self.teamA0Label.text! = "\(teamA0!)"
            self.teamB0Label.text! = "\(teamB0!)"
            
            //SECOND
            let stadium1 = responseArray["Semi-Final"]?[1]["Stadium"];
            let date1 = responseArray["Semi-Final"]?[1]["Date"];
            let time1 = responseArray["Semi-Final"]?[1]["Time"];
            let place1 = responseArray["Semi-Final"]?[1]["Place"];
            let teamA1 = responseArray["Semi-Final"]?[1]["TeamA"];
            let teamB1 = responseArray["Semi-Final"]?[1]["TeamB"];
            
            //setting in labels
            self.stadium1Label.text! = "\(stadium1!)"
            self.date1Label.text! = "\(date1!)"
            self.time1Label.text! = "\(time1!)"
            self.place1Label.text! = "\(place1!)"
            self.imgTeamA1.image = UIImage(named: "netherlands")
            self.imgTeamB1.image = UIImage(named: "sweden")
            self.teamA1Label.text! = "\(teamA1!)"
            self.teamB1Label.text! = "\(teamB1!)"
            
            
            
        }
        //let stadiums = [stadium0, stadium1, stadium2, stadium3]
    }

}
